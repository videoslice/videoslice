# Getting Started

# Prerequesites

- [.NET CLI](https://docs.microsoft.com/en-us/dotnet/core/install/sdk?pivots=os-macos)
- [Docker](https://docs.docker.com/install/)



Load the project in VSCode and press F5. The project should run automatically. 
If not, follow these steps to get running:

1. under Client directory run `npm install`
2. under Client directory run `npm run build`
3. under Server directory run `dotnet build`


# Client

Under the Client directory you can run:

### `npm start`

Runs the app in the development mode independently from the backend.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.


### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.


# Server

When running, open localhost:5001/swagger to view API documentation.

Under the Server directory you can run:

### `dotnet build`

Builds the ASP.NET backend project.


### `dotnet start` 

Starts the backend independently.


# RabbitMQ

To get RabbitMQ running, all you need to do is use the docker-compose command line interface in the root folder. After starting the RabbitMQ container you may need to enable to management plugin to view the admin page at localhost:15672. 

Do this by running: `docker-compose exec rabbitmq rabbitmq-plugins enable rabbitmq_management`

### `docker-compose up -d`

Starts all of the containers. Currently includes MySQL database, and RabbitMQ broker.

### `docker-compose down` 

Stops all project containers.

### `docker-compose logs <container-name>`

Shows the logs of a specific container.