import axios from 'axios';

export default class DataService {

    private _baseUrl = "http://localhost:5000/";

    public post = (route: string, body: object) => {
        let headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        };
        return axios.post(this._baseUrl + route, body, { headers: headers });
    }

    public get = (route: string) => {
        return axios.get(this._baseUrl + route);
    }

    /**
     *  Puts a file to an external endpoint. Does not use base url.
     */
    public putExternal = (externalUrl: string, file: File) => {
        let contentType = file.type;

        let headers = {
            'Content-Type': contentType
        }
        return axios.put(externalUrl, file, { headers: headers });
    }
}