import React, { Component } from 'react';
import './App.css';
import DataService from './DataService';

interface IProps {
}

interface IState {
  videoStreamUrl: string,
  keyName: string,
  file?: File
}

interface ISignedUrlResponse {
  signedUrl: string;
}

export class App extends Component<IProps, IState>
{
  private dataService: DataService;
  private cdnUrl = "https://d6b0gufajt1in.cloudfront.net/"

  constructor(props: IProps) {
    super(props);
    this.state = {
      videoStreamUrl: '',
      keyName: '',
      file: undefined
    }
    this.dataService = new DataService();
  }

  handleFileChange = (files: FileList | null) => {
    if (files) {
      this.setState({file: files[0]});
    }
  }

  uploadFile = (uploadUrl: string, keyName: string, file: File) => {
    return this.dataService.putExternal(uploadUrl, file).then((res) => {
      return res.status === 200;
    }).catch((e) => { throw e; })
  }

  getUploadUrl = (keyName: string, file: File): Promise<string>  => {
    return this.dataService.post("api/S3/UploadUrlRequest", { 'keyName': keyName, 'contentType': file.type })
    .then((res) => {
      let response = res.data as ISignedUrlResponse;
      return response.signedUrl;
    }).catch((e) => { throw e; });
  }

  handleUploadClick = async () => {
    if (!this.state.keyName || !this.state.file) {
      alert("keyname and file required");
      return;
    }

    let uploadUrl = await this.getUploadUrl(this.state.keyName, this.state.file);
    let uploadSuccess = await this.uploadFile(uploadUrl, this.state.keyName, this.state.file);

    if (!uploadSuccess) {
      alert("failed to upload file");
      return;
    }

    this.setState({ videoStreamUrl: this.cdnUrl + this.state.keyName });
  }
  
  render() {
    const keyName = this.state.keyName;
    return (
      <div className="App">
        {keyName}
        <input placeholder="Object Key Name" onChange={(event) => this.setState({ keyName: event.target.value})}/>
        <input type="file" accept="video/*" onChange={(event) => this.handleFileChange(event.target.files)}/>
        <button onClick={() => this.handleUploadClick()}>Upload</button>
        
        <video controls src={this.state.videoStreamUrl} className="video"></video> 
      </div>
    );
  }
}