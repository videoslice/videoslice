
namespace my_new_app.Services {
    public interface IS3Service {
        string GetPresignedUploadUrl(string keyName, string contentType);
        string GetPresignedStreamUrl(string keyName);
    }
}