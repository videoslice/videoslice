

using System;
using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.Extensions.Configuration;

namespace my_new_app.Services {
    public class S3Service : IS3Service {

        private IAmazonS3 _S3Client;
        private string _bucketName;

        public S3Service(IAmazonS3 S3Client, IConfiguration configuration)
        {
            _S3Client = S3Client;
            _bucketName = configuration.GetValue<string>("bucketName");
        }

        public string GetPresignedUploadUrl(string keyName, string contentType) {
            GetPreSignedUrlRequest request = new GetPreSignedUrlRequest() {
                BucketName = _bucketName, 
                Key = keyName,  
                Expires = DateTime.Now.AddMinutes(5),
                Verb = HttpVerb.PUT,
                ContentType = contentType
            };

            return _S3Client.GetPreSignedURL(request);
        }

        public string GetPresignedStreamUrl(string keyName) {
            GetPreSignedUrlRequest request = new GetPreSignedUrlRequest() {
                BucketName = _bucketName,
                Key = keyName,
                Expires = DateTime.Now.AddMinutes(20),
                Verb = HttpVerb.GET
            };

            return _S3Client.GetPreSignedURL(request);
        }
    }
}