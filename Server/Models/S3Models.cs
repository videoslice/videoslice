public class SignedUrlRequest
{
    public string keyName { get; set; }
    public string contentType { get; set; }
}

public class SignedUrlResponse
{
    public string signedUrl { get; set; }
}