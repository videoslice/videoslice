using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using Pomelo.EntityFrameworkCore.MySql.Storage;

namespace my_new_app
{
    public class AppContext : DbContext
    {
        public AppContext(DbContextOptions<AppContext> contextOptions) : base(contextOptions)
        {
        }
        public AppContext()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql("server=127.0.0.1,port=3306;database=db;user=user;password=password;", mySqlOptions => mySqlOptions
                    .ServerVersion(new ServerVersion(new Version(8, 0, 19), ServerType.MySql))
                );
        }
        public DbSet<User> Users { get; set; }
    }

    public class User
    {
        [Key]
        public int user_id { get; set; }
        public string email { get; set; }
    }
}