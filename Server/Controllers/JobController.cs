using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using my_new_app.Services;

namespace my_new_app.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class JobController : Controller
    {
        private readonly ILogger<S3Controller> _logger;
        private IRabbitManager _manager;
        private IS3Service _S3Service;

        // This will be the controller that handles creating, and modifying? jobs
        public JobController(ILogger<S3Controller> logger, IRabbitManager manager)
        {
            _logger = logger;
            _manager = manager;
        }

        [HttpGet]
        public IActionResult Go() {
            try {
                _manager.Publish("Hello World.", "amq.direct", "direct", "VideoSlice-Test");
                return Ok();
            } 
            catch (Exception e) {
                throw e;
            }
        }
    }
}