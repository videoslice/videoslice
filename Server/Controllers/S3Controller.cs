﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using my_new_app.Services;

namespace my_new_app.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class S3Controller : Controller
    {
        private readonly ILogger<S3Controller> _logger;
        private IS3Service _S3Service;

        public S3Controller(ILogger<S3Controller> logger, IS3Service S3Service)
        {
            _logger = logger;
            _S3Service = S3Service;
        }


        [HttpPost]
        public IActionResult UploadUrlRequest([FromBody] SignedUrlRequest request) {
            try {
                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }

                string uploadUrl = _S3Service.GetPresignedUploadUrl(request.keyName, request.contentType);

                SignedUrlResponse response = new SignedUrlResponse() {
                    signedUrl = uploadUrl
                };

                return Ok(response);
            }
            catch (Exception e) {
                throw e;
            }
        }


        [HttpPost]
        public IActionResult StreamUrlRequest([FromBody] SignedUrlRequest request) {
            try {
                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }

                string streamUrl = _S3Service.GetPresignedStreamUrl(request.keyName);

                SignedUrlResponse response = new SignedUrlResponse() {
                    signedUrl = streamUrl
                };

                return Ok(response);
            }
            catch (Exception e) {
                throw e;
            }
        }
    }
}