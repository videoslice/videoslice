using Amazon;
using Amazon.S3;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.ObjectPool;
using my_new_app.Services;
using RabbitMQ.Client;

namespace my_new_app
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called at runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllersWithViews();

            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "../Client/build";
            });

            services.AddSwaggerDocument();  // localhost:XXXX/swagger
                    
            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "../Client/public";
            });

            services.AddDbContextPool<AppContext>(options => {});

            // RabbitMQ services
            var rabbitConfig = Configuration.GetSection("rabbit");
            services.Configure<RabbitOptions>(rabbitConfig);  
            services.AddSingleton<ObjectPoolProvider, DefaultObjectPoolProvider>();  
            services.AddSingleton<IPooledObjectPolicy<IModel>, RabbitModelPooledObjectPolicy>();  
            services.AddSingleton<IRabbitManager, RabbitManager>();
            services.AddSingleton <IConfiguration>(Configuration);

            services.AddSingleton<IS3Service, S3Service>((ctx) =>
            {
                // S3Service Singleton creation
                var configuration = ctx.GetService<IConfiguration>();
                var S3Client = new AmazonS3Client(
                    configuration.GetValue<string>("AWSAccessKey"), 
                    configuration.GetValue<string>("AWSSecretKey"), 
                    RegionEndpoint.USWest2);
                return new S3Service(S3Client, configuration);
            }); 
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseOpenApi(); // serve OpenAPI/Swagger documents

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwaggerUi3();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
                app.UseSpaStaticFiles();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "../Client/";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });
        }
    }
}
